package com.ardinal.task;

import java.util.Arrays;

public class Duplicate {

    private int duplicatePartition(int arr[]){
        int n = arr.length;

        if (n==0 || n==1){
            return n;
        }
        int[] temp = new int[n];
        int j = 0;
        for (int i=0; i<n-1; i++){
            if (arr[i] != arr[i+1]){
                temp[j++] = arr[i];
            }
        }
        temp[j++] = arr[n-1];

        if (j >= 0) System.arraycopy(temp, 0, arr, 0, j);
        return j;
    }

    void removeDuplicate(int[] arr) {
        Arrays.sort(arr);
        int length = duplicatePartition(arr);
        for (int i=0; i<length; i++)
            System.out.print(arr[i]+" ");
    }
}
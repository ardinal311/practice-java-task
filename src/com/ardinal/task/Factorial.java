package com.ardinal.task;

public class Factorial {

    private int number, result = 1;

    public Factorial(int number) {
        this.number = number;
    }

    public void loopFactorial() {
        for (int i=1; i <= number; i++) {
            result = result * i;
        }
        showResult(result);
    }

    public int recursiveFactorial(int n)  {
        if (n == 0) {
            return 1;
        }
        else {
            return n * recursiveFactorial(n - 1);
        }
    }

    public void showResult(int result) {
        System.out.println("Faktorial dari: " + number + " adalah " + result);
    }
}

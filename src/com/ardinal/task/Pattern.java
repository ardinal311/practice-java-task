package com.ardinal.task;

class Pattern {

    private int num;

    public Pattern(int numOfRows) {
        this.num = numOfRows;
    }

    void printAll() {
        pattern1(); pattern2(); pattern3(); pattern4(); pattern5(); pattern6(); pattern7(); pattern8(); pattern9(); pattern10();
        pattern11(); pattern12(); pattern13(); pattern14(); pattern15(); pattern16(); pattern17(); pattern18(); pattern19(); pattern20();
        pattern21(); pattern22(); pattern23(); pattern24(); pattern25(); pattern26(); pattern27(); pattern28(); pattern29(); pattern30();
        pattern31(); pattern32(); pattern33(); pattern34(); pattern35(); pattern36(); pattern37(); pattern38(); pattern39(); pattern40();
        pattern41(); pattern42(); pattern43(); pattern44(); pattern45(); pattern46(); pattern47(); pattern48(); pattern49();
    }


    void pattern1() {
        System.out.println("Pattern 1 :");
        for(int i = 1; i <= num; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(j + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    void pattern2() {
        System.out.println("Pattern 2 :");
        for (int i = 1; i <= num; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(i + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    void pattern3() {
        System.out.println("Pattern 3 :");
        for(int i = 1; i <= num; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(j + " ");
            }
            System.out.println();
        }
        for(int i = num - 1; i >= 1; i--) {
            for (int j = 1; j <= i; j++) {
                System.out.print(j + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    void pattern4() {
        System.out.println("Pattern 4 :");
        for(int i = num; i >= 1; i--) {
            for (int j = 1; j <= i; j++) {
                System.out.print(j + " ");
            }
            System.out.println();
        }

        for(int i = 1; i <= num; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(j + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    void pattern5() {
        System.out.println("Pattern 5 :");

        for(int i = num; i >= 1; i--) {
            for (int j = i; j >= 1; j--) {
                System.out.print(j + " ");
            }
            System.out.println();
        }

        for(int i = 1; i <= num; i++) {
            for (int j = i; j >= 1; j--) {
                System.out.print(j + " ");
            }
            System.out.println();
        }

        System.out.println();
    }

    void pattern6() {
        System.out.println("Pattern 6 :");
        for (int i = 1; i <= num; i++) {
            for (int j = num; j > i; j--) {
                System.out.print(" ");
            }

            for (int k = 1; k <= i; k++) {
                System.out.print(k + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    void pattern7() {
        System.out.println("Pattern 7 :");
        for (int i = 1; i <= num; i++) {
            for (int j = num; j >= i; j--) {
                System.out.print(j + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    void pattern8() {
        System.out.println("Pattern 8 :");
        for (int i = num; i >= 1; i--) {
            for (int j = num; j >= i; j--) {
                System.out.print(j + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    void pattern9() {
        System.out.println("Pattern 9 :");
        for(int i = num; i >= 1; i--) {
            for (int j = 1; j <= i; j++) {
                System.out.print(j + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    void pattern10() {
        System.out.println("Pattern 10 :");
        int number = 0;
        for(int i = 1; i <= num; i++) {
            for (int j = 1; j <= i; j++) {
                number = number + 1;
                System.out.print(number + " ");
            }
            System.out.println();
        }
    }

    void pattern11() {
        System.out.println("Pattern 11 :");
        for(int i = 1; i <= num; i++) {
            for (int j = i; j >= 1; j--)
            {
                System.out.print(j + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    void pattern12() {
        System.out.println("Pattern 12 :");
        for(int i = 1; i <= num; i++) {
            for(int j = i; j >= 1; j--) {
                System.out.print(j + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    void pattern13() {
        System.out.println("Pattern 13 :");
        for (int i = 1; i <= num; i++) {
            for (int j = num; j > i; j--) {
                System.out.print(" ");
            }

            int temp = 1;

            for (int k = 1; k <= i; k++) {
                System.out.print(temp + " ");
                temp = temp * (i - k) / (k);
            }

            System.out.println();
        }
        System.out.println();
    }

    void pattern14() {
        System.out.println("Pattern 14 :");
        for (int i = 1; i <= num; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(j + " ");
            }

            for (int k = i - 1; k >= 1; k--) {
                System.out.print(k + " ");
            }

            System.out.println();
        }
        System.out.println();
    }

    void pattern15() {
        System.out.println("Pattern 15 :");
        for (int i = num; i >= 1; i--) {
            for (int j = num; j > i; j--) {
                System.out.print(" ");
            }
            for(int k = 1; k <= i; k++) {
                System.out.print(k + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    void pattern16() {
        System.out.println("Pattern 16 :");
        for (int i = 1; i <= num; i++) {
            for (int j = num; j > i; j--) {
                System.out.print(" ");
            }
            for (int k = 1; k <= i; k++) {
                System.out.print(k + " ");
            }
            System.out.println();
        }

        for (int i = num - 1; i >= 1; i--) {
            for (int j = i; j < num; j++) {
                System.out.print(" ");
            }
            for (int k = 1; k <= i; k++) {
                System.out.print(k + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    void pattern17() {
        System.out.println("Pattern 17 :");
        for (int i = 1; i <= num; i++) {
            for (int j = 1; j < i; j++) {
                System.out.print(" ");
            }
            for (int k = i; k <= num; k++) {
                System.out.print(k);
            }
            System.out.println();
        }

        for (int i = num; i >= 1; i--) {
            for (int j = 1; j < i; j++) {
                System.out.print(" ");
            }
            for (int k = i; k <= num; k++) {
                System.out.print(k);
            }
            System.out.println();
        }
        System.out.println();
    }

    void pattern18() {
        System.out.println("Pattern 18 :");
        for (int i = 1; i <= num; i++) {
            for (int j = 1; j < i; j++) {
                System.out.print(" ");
            }
            for (int k = i; k <= num; k++) {
                System.out.print(k + " ");
            }
            System.out.println();
        }

        for (int i = num; i >= 1; i--) {
            for (int j = 1; j < i; j++) {
                System.out.print(" ");
            }
            for (int k = i; k <= num; k++) {
                System.out.print(k + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    void pattern19() {
        System.out.println("Pattern 19 :");
        for (int i = num; i >= 1; i--) {
            for (int j = 1; j < i; j++) {
                System.out.print(" ");
            }

            for (int k = i; k <= num; k++) {
                System.out.print(k + " ");
            }

            System.out.println();
        }
        System.out.println();
    }

    void pattern20() {
        System.out.println("Pattern 20 :");
        for (int i = 1; i <= num; i++) {
            for (int j = num; j > i; j--) {
                System.out.print("  ");
            }

            for (int k = 1; k <= i; k++) {
                System.out.print(k + " ");
            }

            for (int l = i - 1; l >= 1; l--) {
                System.out.print(l + " ");
            }

            System.out.println();
        }
        System.out.println();
    }

    void pattern21() {
        System.out.println("Pattern 21 :");
        for (int i = 1; i<=num; i++) {
            for (int j = 1; j<=i; j++) {
                if (j % 2 == 0) {
                    System.out.print(0 + " ");
                }
                else {
                    System.out.print(1 + " ");
                }
            }
            System.out.println();
        }
        System.out.println();
    }

    void pattern22() {
        System.out.println("Pattern 22 :");
        for (int i = 1; i<=num; i++) {
            for (int j = 1; j<i; j++) {
                System.out.print("0 ");
            }
            System.out.print(i + " ");
            for (int k = num; k > i; k--) {
                System.out.print("0 ");
            }
            System.out.println();
        }
        System.out.println();
    }

    void pattern23() {
        System.out.println("Pattern 23 :");

        for (int i = 1; i<=num; i++) {
            for (int j = num; j > i; j--) {
                System.out.print("1 ");
            }
            for (int k = 1; k <= i; k++) {
                System.out.print(i+ " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    void pattern24() {
        System.out.println("Pattern 24 :");

        for (int i = 1; i <= num; i++) {
            for (int j = i; j <= num; j++) {
                System.out.print(j + " ");
            }
            for (int k = num - 1; k >= i; k--) {
                System.out.print(k+ " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    void pattern25() {
        System.out.println("Pattern 25 :");

        for (int i= 1; i <= num; i++) {
            for (int j = num - 1; j >= i; j--) {
                System.out.print(" ");
            }
            for (int k = 1; k <= i; k++) {
                System.out.print(i + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    void pattern26() {
        System.out.println("Pattern 26 :");

        for (int i = num; i >= 1; i--) {
            for (int j = i; j < num; j++) {
                System.out.print(j + " ");
            }

            for (int k = num - i; k < num; k++) {
                System.out.print(5 + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    void pattern27() {
        System.out.println("Pattern 27 :");

        int k = 1;

        for (int i = 1; i <= num; i++) {
            k=i;
            for (int j = 1; j <= i; j++) {
                System.out.print(k + " ");
                k = k + num - j;
            }
            System.out.println();
        }
        System.out.println();
    }

    void pattern28() {
        System.out.println("Pattern 28 :");

        int temp = 1;

        for(int i=1; i<=num/2+1; i++) {
            for(int j=1;j<=i;j++) {
                System.out.print(temp*j+" ");
            }
            System.out.println();
            temp++;
        }

        for(int i=1; i<=num/2; i++) {
            for(int j=1;j<=num/2+1-i;j++) {
                System.out.print(temp*j+" ");
            }
            System.out.println();
            temp++;
        }
        System.out.println();
    }

    void pattern29() {
        System.out.println("Pattern 29 :");

        for (int i = 0; i < num; i++) {
            for (int j = 0; j <= i; j++) {
                int i1 = 1 + j * num - (j - 1) * j / 2;

                if (j % 2 == 0) {
                    System.out.print(i1 + i - j + " ");
                }
                else {
                    System.out.print(i1 + num - 1 - i + " ");
                }
            }
            System.out.println();
        }
        System.out.println();
    }

    void pattern30() {
        System.out.println("Pattern 30 :");

        for (int i = 0; i < num; i++) {
            for (int j = 0; j < num; j++) {
                if (j % 2 == 0)
                    System.out.print((num * (j)) + i + 1 + " ");
                else
                    System.out.print((num * (j + 1)) - i + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    void pattern31() {
        System.out.println("Pattern 31 :");
        int temp = 0;

        for (int i = num; i >= 1; i--) {
            for (int j = num ; j >= i; j--) {
                System.out.print(j + " ");
                temp =j;
            }

            for (int k = num - i+1; k < num; k++) {
                System.out.print(temp + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    void pattern32() {
        System.out.println("Pattern 32 :");

        for (int i = 1; i <= num; i++) {
            int a = 0;
            int b = 1;

            for (int j = 1; j <= i; j++) {
                int c = a + b;
                System.out.print(c + " ");
                a = b;
                b = c;
            }

            System.out.println();
        }
        System.out.println();
    }

    void pattern33() {
        System.out.println("Pattern 33 :");

        for (int i = num; i >= 1; i--) {
            for (int j = i; j <= num; j++) {
                System.out.print(j + " ");
            }
            for (int k = num-1; k >= i; k--) {
                System.out.print(k + " ");
            }
            System.out.println();
        }

        for (int i = 2; i <= num; i++) {
            for (int j = i; j <= num; j++) {
                System.out.print(j + " ");
            }
            for (int k = num-1; k >= i; k--) {
                System.out.print(k + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    void pattern34() {
        System.out.println("Pattern 34 :");

        for (int i = 1; i <= num; i++) {
            int j = i;

            for (int k = 1; k <= num; k++) {
                System.out.print(j + " ");
                j++;
                if (j > num)
                    j = 1;
            }
            System.out.println();
        }
        System.out.println();
    }

    void pattern35() {
        System.out.println("Pattern 35 :");

        for (int i = 1; i <= num; i++) {
            for (int j = i; j <= num; j++) {
                System.out.print(j + " ");
            }

            for(int k = i-1; k >= 1; k--) {
                System.out.print(k + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    void pattern36() {
        System.out.println("Pattern 36 :");

        for (int i = 1; i <= num; i++) {
            int j = (i * 2) - 1;

            for (int k = 1; k <= num; k++) {
                System.out.print(j + " ");
                j += 2;
                if (j > (num * 2) - 1)
                    j = 1;
            }
            System.out.println();
        }
        System.out.println();
    }

    void pattern37() {
        System.out.println("Pattern 37 :");

        for (int i = 1; i <= num; i++) {
            int j = (i * 2) - 1;

            for (int k = i; k <= num; k++) {
                System.out.print(j + " ");
                j += 2;
            }

            for (int l = (i * 2) - 3; l >= 1; l-=2) {
                System.out.print(l + " ");
            }

            System.out.println();
        }
        System.out.println();
    }

    void pattern38() {
        System.out.println("Pattern 38 :");

        for (int i = 1; i <= num; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(j);
            }

            for (int j= i*2 ; j < num*2; j++) {
                System.out.print(" ");
            }

            for (int l = i; l >= 1; l--) {
                System.out.print(l);
            }

            System.out.println();
        }
        System.out.println();
    }

    void pattern39() {
        System.out.println("Pattern 39 :");

        int currentRow = 1;
        int counter = 1;

        for (int i=1; i<= num; i++) {
            if (i % 2 == 0) {
                int reverse = currentRow + counter - 1;
                for (int j = 0; j < i; j++) {
                    System.out.print(reverse-- + " ");
                    counter++;
                }
            } else {
                for (int j = 1; j <= i; j++) {
                    System.out.print(counter + " ");
                    counter++;
                }
            }
            System.out.println();
            currentRow++;
        }
        System.out.println();
    }

    void pattern40() {
        System.out.println("Pattern 40 :");

        int currentRow = 1;
        int counter = 1;

        for (int i=1; i<= num; i++) {
            if (i % 2 == 0) {
                for (int j = 1; j<=i; j++) {
                    System.out.print(counter  +" ");
                    counter++;
                }
            }
            else {
                 int reverse = currentRow + counter - 1;
                 for (int j = 0; j<i; j++) {
                     System.out.print(reverse--  +" ");
                     counter++;
                 }
            }
            System.out.println();
            currentRow++;
        }
        System.out.println();
    }

    void pattern41() {
        System.out.println("Pattern 41 :");

        for (int i = num; i >= 1; i--) {
            for (int j = num; j >= 1+num-i; j--) {
                System.out.print(j);
            }

            for (int j= i*2 ; j < num*2-1; j++) {
                System.out.print(" ");
            }

            for (int l = 1+num-i; l <=num; l++) {
                if(l!=1)
                    System.out.print(l);
            }
            System.out.println();
        }
        System.out.println();
    }

    void pattern42() {
        System.out.println("Pattern 42 :");

        for (int i = num; i >= 1; i--) {
            for (int j = 1; j <= i; j++) {
                System.out.print(j);
            }

            for (int j= i*2 ; j < num*2-1; j++) {
                System.out.print(" ");
            }

            for (int l = i; l >= 1; l--) {
                if(l!=num)
                    System.out.print(l);
            }
            System.out.println();
        }
        System.out.println();
    }

    void pattern43() {
        System.out.println("Pattern 43 :");

        for (int i = 1; i <= num; i++) {
            for (int j = 1; j <= i; j++) {
                System.out.print(j + " ");
            }

            for (int k = i - 1; k >= 1; k--) {
                System.out.print(k + " ");
            }

            System.out.println();
        }

        for (int i = num-1; i >= 1; i--) {
            for (int j = 1; j <= i; j++) {
                System.out.print(j + " ");
            }
            for (int k = i - 1; k >= 1; k--) {
                System.out.print(k + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    void pattern44() {
        System.out.println("Pattern 44 :");

        for (int i = 1; i <= num; i++) {
            int temp = i;
            for (int j = 1; j <= i; j++) {
                System.out.print(temp + " ");
                temp = temp + 1;
            }

            temp = temp - 2;

            for (int k = i - 1; k >= 1; k--) {
                System.out.print(temp + " ");
                temp = temp - 1;
            }
            System.out.println();
        }

        for (int i = num - 1; i >= 1; i--) {
            int temp = i;

            for (int j = 1; j <= i; j++) {
                System.out.print(temp + " ");
                temp = temp + 1;
            }

            temp = temp - 2;

            for (int k = i - 1; k >= 1; k--) {
                System.out.print(temp + " ");
                temp = temp - 1;
            }

            System.out.println();
        }
        System.out.println();
    }

    void pattern45() {
        System.out.println("Pattern 45 :");
        int temp = 1;
        for (int i = 1; i <= num; i++) {
            for (int k = 1; k <= i; k++) {
                System.out.print(temp + " ");
            }

            temp++;

            System.out.println();
        }

        for (int i = num - 1; i >= 1; i--) {
            for (int k = i; k >= 1; k--) {
                System.out.print(temp + " ");
            }

            temp++;
            System.out.println();
        }
        System.out.println();
    }

    void pattern46() {
        System.out.println("Pattern 46 :");

        for (int i = 1; i <= num; i++) {
            for (int k = i; k >= 1; k--) {
                System.out.print(k + " ");
            }

            for (int l = 2; l <= i; l++) {
                System.out.print(l + " ");
            }

            System.out.println();
        }

        for (int i = num - 1; i >= 1; i--) {
            for (int k = i; k >= 1; k--) {
                System.out.print(k + " ");
            }
            for (int l = 2; l <= i; l++) {
                System.out.print(l + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    void pattern47() {
        System.out.println("Pattern 47 :");

        for (int i = 1; i <= num; i++) {
            for (int j = num; j > i; j--) {
                System.out.print(" ");
            }

            int val1 = 1;

            for (int k = 1; k <= i; k++) {

                System.out.print(val1 + " ");
                val1 = val1 * 2;
            }

            val1 = val1 / 4;

            for (int l = i - 1; l >= 1; l--) {
                System.out.print(val1 + " ");
                val1 = val1 / 2;
            }
            System.out.println();
        }
        System.out.println();
    }

    void pattern48() {
        System.out.println("Pattern 48 :");
        for (int i = num; i >= 1; i--) {
            for (int j = i; j <= num; j++) {
                System.out.print(j + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    void pattern49() {
        System.out.println("Pattern 49 :");

        int temp = 2;
        Prime prime = new Prime();

        for (int i = 1; i <= num; i++) {
            for (int j = 1; j <= i; j++) {
                while (!prime.isPrime(temp)) {
                    temp++;
                }
                System.out.print(temp + " ");
                temp++;
            }
            System.out.println();
        }
        System.out.println();
    }
}

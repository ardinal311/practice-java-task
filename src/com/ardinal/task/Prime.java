package com.ardinal.task;

class Prime {

    boolean isPrime(int number) {
        if (number <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(number); ++i) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }

    void checkPrime(int number) {
        if (isPrime(number)) {
            System.out.println("angka " + number + " adalah bilangan Prima");
        }
        else {
            System.out.println("angka " + number + " bukan bilangan Prima");
        }
    }
}

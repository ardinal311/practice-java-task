package com.ardinal.task;

class Fibonacci {
    private int result;

    void loopFibonacci(int max) {
        int prev = 1;
        int next = 1;
        System.out.println("Fibonacci for-loop " + max + " nomor: ");
        System.out.print(prev + " " + next + " ");
        for (int i = 2; i < max; ++i) {
            result = prev + next;
            prev = next;
            next = result;
            System.out.print(result + " ");
        }
        System.out.println();
    }

    private int getFibonacci(int max) {
        if (max == 0) {
            return 0;
        }
        else if (max == 1) {
            return 1;
        }
        else {
            return getFibonacci(max-1) + getFibonacci(max - 2);
        }
    }

    void recFibonacci(int max) {
        System.out.println("Fibonacci recursion " + max + " nomor: ");
        for (int i = 1; i <= max; ++i) {
            result = getFibonacci(i);
            System.out.print(result + " ");
        }
        System.out.println();
    }
}

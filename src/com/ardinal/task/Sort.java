package com.ardinal.task;

class Sort {

    private int quickSortPartition(int[] array, int low, int high) {
        int pivot = array[high];
        int i = low - 1;

        for (int j = low; j < high; j++) {
            if (array[j] <= pivot) {
                i++;
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
        }
        int temp = array[i + 1];
        array[i + 1] = array[high];
        array[high] = temp;
        return i + 1;
    }

    void quickSort(int[] array, int low, int high) {
        if (low < high) {
            int partition = quickSortPartition(array, low, high);
            quickSort(array,  low, partition - 1);
            quickSort(array, partition + 1, high);
        }
    }

    void printQuickSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }

    void insertionSort(int[] array) {
        int length = array.length;
        int holePosition;
        int valueToInsert;

        for (int i = 1; i < length; i++) {
            valueToInsert = array[i];
            holePosition = i;
            while (holePosition > 0 && array[holePosition-1] > valueToInsert) {
                array[holePosition] = array[holePosition - 1];
                holePosition = holePosition - 1;
            }
            array[holePosition] = valueToInsert;
        }

        for (int value : array) {
            System.out.print(value + " ");
        }
    }

    void bubbleSort(int[] array) {
        int length = array.length;
        int temp;

        for (int i = 0; i < length; i++) {
            for (int j = 0; j < (length - i); j++) {
                if (j == (length - 1)) {
                    break;
                }
                else {
                    if (array[j] > array[j+1]) {
                        temp = array[j];
                        array[j] = array[j+1];
                        array[j+1] = temp;
                    }
                }
            }
        }
        for (int value : array) {
            System.out.print(value + " ");
        }
    }
}

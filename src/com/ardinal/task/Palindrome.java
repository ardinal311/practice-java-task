package com.ardinal.task;

class Palindrome {

    void stringPalindrome(String sentence) {
        String reverse = "";

        int length = sentence.length();

        for (int i = length - 1; i >= 0; i--) {
            reverse = String.format("%s%s", reverse, sentence.charAt(i));
        }

        if (sentence.equals(reverse)) {
            System.out.println(sentence + " adalah string palindrome");
        }
        else {
            System.out.println(sentence + " bukan string palindrome");
        }
    }

    void intPalindrome(int number) {
        int sum = 0;
        int counter = number;

        while (counter > 0 ) {
            sum = (sum*10) + (counter%10);
            counter /= 10;
        }
        if (number == sum) {
            System.out.println(number + " adalah angka palindrome.");
        }
        else {
            System.out.println(number + " bukan angka palindrome.");
        }
    }
}

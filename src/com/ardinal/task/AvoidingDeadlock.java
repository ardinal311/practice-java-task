package com.ardinal.task;

public class AvoidingDeadlock {

    public AvoidingDeadlock(){}

    public void deadMethod1() {
        synchronized (Integer.class) {
            System.out.println("TASK 1 : Aquired lock on Integer.class object");
            synchronized (String.class) {
                System.out.println("TASK 1 : Aquired lock on String.class object");
            }
        }
    }

    public void deadMethod2() {
        synchronized (Integer.class) {
            System.out.println("TASK 2 : Aquired lock on Integer.class object");
            synchronized (String.class) {
                System.out.println("TASK 2 : Aquired lock on String.class object");
            }
        }
    }
}

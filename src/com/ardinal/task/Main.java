package com.ardinal.task;

public class Main {
    public static void main(String [] args) {
        System.out.println("1. Fibonacci: ");
        Fibonacci fibo = new Fibonacci();
        fibo.loopFibonacci(10);
        System.out.print("\n");
        fibo.loopFibonacci(2);
        System.out.print("\n");
        fibo.recFibonacci(5);
        System.out.print("\n");
        fibo.recFibonacci(4);
        System.out.println("\n");

//        System.out.println("2. Check Prime Number: ");
//        Prime prime = new Prime();
//        prime.checkPrime(4);
//        prime.checkPrime(7);
//        System.out.print("\n");
//
//        System.out.println("3. Check String Palindrome: ");
//        Palindrome palindrome = new Palindrome();
//        palindrome.stringPalindrome("Uvuvwevwevwe Onyetenyevwe Ugwemubwem Osas");
//        palindrome.stringPalindrome("AbiibA");
//        System.out.print("\n");
//
//        System.out.println("4. Check Int Palindrome: ");
//        palindrome.intPalindrome(3003);
//        palindrome.intPalindrome(2020);
//        System.out.print("\n");
//
        System.out.println("5. Check Armstrong Number: ");
        Armstrong arm1 = new Armstrong(371);
        arm1.checkArmstrong();
        Armstrong arm2 = new Armstrong(567);
        arm2.checkArmstrong();
        System.out.print("\n");
//
//        System.out.println("5. Factorial Number:");
//        Factorial fac1 = new Factorial(4);
//        fac1.loopFactorial();
//        int recResult = fac1.recursiveFactorial(5);
//        fac1.showResult(recResult);
//
//        AvoidingDeadlock av1 = new AvoidingDeadlock();
//        av1.deadMethod1();
//        av1.deadMethod2();
//
//        ReverseString revString = new ReverseString();
//        revString.reversing("abc");
//        System.out.print("\n");

//        Pattern pat = new Pattern(3);
//        pat.printAll();


//        int[] arr = {3, 60, 35, 2, 45, 320, 5};
//        int length = arr.length;
//        Sort bubble = new Sort();
//        bubble.insertionSort(arr);
//        bubble.bubbleSort(arr);
//        bubble.quickSort(arr, 0, length - 1);
//        bubble.printQuickSort(arr);

//        Duplicate duplicate = new Duplicate();
//        int[] arr = {3, 3, 3, 2, 2, 2, 60, 35, 2, 45, 320, 5};
//        duplicate.removeDuplicate(arr);
//
//        ReverseString reverse = new ReverseString();
//        reverse.printReverse("Gula Batu");
//
//        RepeatedChar repeat = new RepeatedChar();
//        repeat.repeated("Programming");
//
//        Permutation permutation = new Permutation();
//        String str = "GOD";
//        permutation.permutation(str, 0 , str.length());
    }
}
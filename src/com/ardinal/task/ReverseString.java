package com.ardinal.task;

public class ReverseString {

    private String reverse = "";

    public void reversing(String sentence) {

        int strLength = sentence.length();

        for (int i = strLength - 1; i >= 0; i--) {
            reverse = reverse + sentence.charAt(i);
        }

        System.out.print(reverse);
    }

    public static String reverse(String str) {
        StringBuilder strBuilder = new StringBuilder();
        char[] strChars = str.toCharArray();

        for (int i = strChars.length - 1; i >= 0; i--) {
            strBuilder.append(strChars[i]);
        }

        return strBuilder.toString();
    }

    public static String reverseRecursively(String str) {

        if (str.length() < 2) {
            return str;
        }

        return reverseRecursively(str.substring(1)) + str.charAt(0);

    }

    public void printReverse(String sentence) {
        String reversed = reverse(sentence);
        System.out.println("Iteration: " + reversed);

        String reverseding = reverseRecursively(sentence);
        System.out.println("Recursive: " + reverseding);
    }
}

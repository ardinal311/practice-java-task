package com.ardinal.task;

public class Permutation {

    String swap(String a, int i, int j) {
        char[] b =a.toCharArray();
        char ch;
        ch = b[i];
        b[i] = b[j];
        b[j] = ch;
        return String.valueOf(b);
    }

    void permutation(String str, int start, int end) {
        if (start == end-1)
            System.out.println(str);
        else {
            for (int i = start; i < end; i++) {
                str = swap(str,start,i);
                permutation(str,start+1,end);
                str = swap(str,start,i);
            }
        }
    }
}

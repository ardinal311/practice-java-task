package com.ardinal.task;

public class Armstrong {

    private int number;
    private int originalNumber;
    private int result = 0;

    public Armstrong(int number) {
        this.number = number;
    }

    public void checkArmstrong() {
        originalNumber = number;
        while (originalNumber != 0) {
            int remainder = originalNumber % 10;
            result += Math.pow(remainder, 3);
            originalNumber /= 10;
        }

        if (result == number) {
            System.out.println(number + " adalah angka Armstrong.");
        }
        else {
            System.out.println(number + " bukan angka Armstrong");
        }
    }
}
